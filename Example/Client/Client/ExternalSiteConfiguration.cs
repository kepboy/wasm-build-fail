﻿using System;
using System.Linq;
using System.Text.Json.Serialization;

namespace Example;

public class ExternalSiteConfiguration
{
    [JsonPropertyName("fundApiBase")]
    public string FundApiBase { get; init; }

    [JsonPropertyName("OrderApiBase")]
    public string OrderApiBase { get; init; }
}
